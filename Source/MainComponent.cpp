/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (600, 400);
    // MIDI In
    audioDeviceManager.setMidiInputEnabled("USB Axiom 49 Port 1", true);
    audioDeviceManager.addMidiInputCallback(String::empty, this);
    // MIDI Out
    audioDeviceManager.setDefaultMidiOutput("SimpleSynth virtual input");
    // MIDI Display
    midiLabel.setText("test", dontSendNotification);
    // combo box
    msgType.addItem("Note On", 1);
    msgType.addItem("Control", 2);
    msgType.addItem("Program", 3);
    msgType.addItem("Pitch Bend", 4);
    msgType.addItem("Note Off", 5);
    // sliders
    channel.setSliderStyle(juce::Slider::IncDecButtons);
    channel.setRange(1, 16);
    number.setSliderStyle(juce::Slider::IncDecButtons);
    number.setRange(0, 127);
    velocity.setSliderStyle(juce::Slider::IncDecButtons);
    velocity.setRange(0, 127);
    //buttons
    sendButton.setButtonText("send");
    sendButton.addListener(this);

    
    // make items visible
    addAndMakeVisible(midiLabel);
    addAndMakeVisible(msgType);
    addAndMakeVisible(channel);
    addAndMakeVisible(number);
    addAndMakeVisible(velocity);
    addAndMakeVisible(sendButton);
    
}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeMidiInputCallback(String::empty, this);
}

void MainComponent::resized()
{
    midiLabel.setBounds(0, 0, getWidth(), 15);
    msgType.setBounds(0, 20, 100, 20);
    channel.setBounds(100, 20, 120, 20);
    number.setBounds(220, 20, 120, 20);
    velocity.setBounds(340, 20, 120, 20);
    sendButton.setBounds(460, 20, 120, 20);
    

}

void MainComponent::handleIncomingMidiMessage(MidiInput* Source, const MidiMessage& message)
{
    String midiText;
    
    if (message.isNoteOnOrOff()) {
        midiText << "NoteOn: Channel " << message.getChannel();
        midiText << " :Number " << message.getNoteNumber();
        midiText << " :Velocity " << message.getVelocity();
    }
    
    audioDeviceManager.getDefaultMidiOutput()->sendMessageNow(message);
    
    midiLabel.getTextValue() = midiText;
    //DBG("message received");
}
void MainComponent::buttonClicked(Button* button)
{
   if (button == &sendButton)
   {
       if (msgType.getSelectedIdAsValue() == 1)
       {
           result = MidiMessage::noteOn(channel.getValue(), number.getValue(), (uint8) velocity.getValue());
       }
       else if(msgType.getSelectedIdAsValue() == 2)
       {
           result = MidiMessage::controllerEvent(channel.getValue(), number.getValue(), velocity.getValue());
       }
       else if (msgType.getSelectedIdAsValue() == 3)
       {
           result = MidiMessage::programChange(channel.getValue(), number.getValue());
       }
       else if (msgType.getSelectedIdAsValue() == 4)
       {
           result = MidiMessage::pitchWheel(channel.getValue(), number.getValue());
       }
       else if (msgType.getSelectedIdAsValue() == 5)
       {
           result = MidiMessage::noteOff(channel.getValue(), number.getValue());
       }
       DBG("sendButton pressed");
       audioDeviceManager.getDefaultMidiOutput()->sendMessageNow(result);
   }
    DBG("general button pressed");
}